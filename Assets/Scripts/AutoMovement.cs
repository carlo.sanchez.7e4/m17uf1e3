﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoMovement : MonoBehaviour
{
    public DataPlayer DP;
    public int count;

    public float distance;
    public float speed;

    public bool movement;
    public float waitTime;

    public Transform female;
    public bool left;
    // Boolean que permet o no activar el poder de Gigantamax
    public bool gigantamax;
    public bool growing;
    public bool degrowing;
    public int growthCounter;

    // Variables de temps
    public float gigantamaxEnd;
    public float gigantamaxCooldown;

    // Start is called before the first frame update
    void Start()
    {
        DP = gameObject.GetComponent<DataPlayer>();
        speed = 4 / DP.Weight;
        distance = DP.Distance / speed;
        Debug.Log(distance);
        count = 0;
        female = gameObject.transform;
        left = true;
        gigantamax = true;
        growing = false;
        degrowing = false;
        movement = true;
        growthCounter = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (movement)
        {
            Movement();
        } else
        {
            if (waitTime + 2 <= Time.time)
            {
                movement = true;
                Flip();
            }
        }

        Gigantamax();
    }

    private void Movement()
    {
        if (left)
        {
            MoveLeft();
        }
        else
        {
            MoveRight();
        }
    }

    private void Gigantamax()
    {
        if (gigantamax)
        {
            if (Input.GetKey(KeyCode.Z))
            {
                growing = true;
                gigantamax = false;
            }
        }
        else if (growing)
        {
            if (growthCounter <= 5 && left)
            {
                female.localScale += new Vector3(1, 1, 0) * 1.1f;
                growthCounter++;
            }
            else if (growthCounter <= 5 && !left)
            {
                female.localScale += new Vector3(-1, 1, 0) * 1.1f;
                growthCounter++;
            }
            else
            {
                degrowing = true;
                gigantamaxEnd = Time.time;
                growing = false;
                growthCounter = 0;
            }
        }
        else if (degrowing)
        {
            if (gigantamaxEnd + 3 <= Time.time)
            {
                if (left)
                {
                    female.localScale = new Vector3(1, 1, 0);
                }
                else
                {
                    female.localScale = new Vector3(-1, 1, 0);
                }

                degrowing = false;
                gigantamaxCooldown = Time.time;
            }
        }
        else
        {
            if (gigantamaxCooldown + 5 <= Time.time)
            {
                gigantamax = true;
            }
        }
    }

    private void MoveLeft()
    {
        if (count <= distance)
        {
            female.position += Vector3.left * speed;
            count++;
        }
        else
        {
            left = false;
            count = 0;
            waitTime = Time.time;
            movement = false;
        }
    }

    private void MoveRight()
    {
        if (count <= distance)
        {
            female.position += Vector3.right * speed;
            count++;
        }
        else
        {
            left = true;
            count = 0;
            waitTime = Time.time;
            movement = false;
        }
    }

    protected void Flip()
    {
        Vector3 theScale = female.localScale;
        theScale.x *= -1;
        female.localScale = theScale;
    }
}
