﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteAnimation : MonoBehaviour
{
    private Sprite[] animationSprites;
    private int animationSpritesIndex;

    //Save Sprite var to SpriteRenderer
    private SpriteRenderer spriteRenderer;
    
    // Start is called before the first frame update
    void Start()
    {
        animationSprites = gameObject.GetComponent<DataPlayer>().spriteArray;
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();

        spriteRenderer.sprite = animationSprites[0];
        animationSpritesIndex = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKey)
        {
            Animate();
        } else
        {
            spriteRenderer.sprite = animationSprites[1];
        }
    }

    public void Animate()
    {
        spriteRenderer.sprite = animationSprites[animationSpritesIndex];
        animationSpritesIndex++;

        if (animationSpritesIndex >= animationSprites.Length)
        {
            animationSpritesIndex = 0;
        }
    }
}
